const Commando = require('discord.js-commando');
const path = require('path');
const sqlite = require('sqlite');

const config = require('./config/config.json');

// Init client
const client = new Commando.Client({
  owner: config.ownerid,
  commandPrefix: config.prefix,
  disableEveryone: true,
  unknownCommandResponse: false
});

client.registry
  // Registers your custom command groups
  .registerGroups([
    ['channels', 'Puhekanavan työkalut'],
    ['util', 'Yleiset komennot']
])

  // Registers all built-in groups, some commands, and argument types
  // .registerDefaultGroups()
  .registerDefaultTypes()
  .registerDefaultCommands({
    help: false,
    prefix: false,
    eval_: true,
    ping: false,
    commandState: false
  })

  // Registers all of your commands in the ./commands/ directory
  .registerCommandsIn(path.join(__dirname, 'commands'));



client.on('ready', () => {
  console.log(`\nLogged in:
Bot: ${client.user.tag} / ${client.user.id})`);
  client.user.setActivity("vittu", {
      type: 'WATCHING'
    })
    .then(presence => console.log(`Activity set.`))
    .catch(console.error);
});

client.setProvider(
    sqlite.open(path.join(__dirname, 'settings.sqlite3')).then(db => new Commando.SQLiteProvider(db))
).catch(console.error);

client.login(config.token)