const {
    Command
} = require('discord.js-commando');
const config = require('./../../config/config.json');

module.exports = class ReplyCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'tuhoa',
            group: 'channels',
            memberName: 'tuhoa',
            description: 'Asettaa pelaajan puhekanavan.',
            examples: ['tuhoa'],
            aliases: ['del', 'd'],
            args: [{
                key: 'member',
                prompt: 'Kenen rooli?',
                type: 'member'
            }]
        });
    }

    run(msg, args) {
        const roleIds = msg.guild.settings.get("roles");
        const roles = msg.member.roles;
        const roleId = roleIds[args.member.id];

        const channelIds = msg.guild.settings.get("channels");
        const channelId = channelIds[args.member.id];

        if (roles.find(role => role.name.toLowerCase() === config.developer)) { // role exists

            // poista kanava databasesta
            delete channelIds[args.member.id];
            msg.guild.settings.set("channels", channelIds)
            // poista role databasesta
            delete roleIds[args.member.id];
            msg.guild.settings.set("roles", roleIds)

            // poista role
            msg.guild.roles.get(roleId).delete().then(function () {
                // poista kanava
                msg.guild.channels.get(channelId).delete().then(
                    function () {
                        msg.channel.send("Pelaajan tiedot poistettu!");
                    }
                )
            })
        } else {
            msg.channel.send("Sinulla ei ole oikeuksia.")
        }
    }
};