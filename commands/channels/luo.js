const {
    Command
} = require('discord.js-commando');
const config = require('./../../config/config.json');

/*
 *
 *
 * AIVAN JÄÄTÄVÄÄ
 * SPAGETTIA
 * 
 * 
 */

module.exports = class ReplyCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'luo',
            group: 'channels',
            memberName: 'luo',
            description: 'Asettaa pelaajan puhekanavan.',
            examples: ['luo'],
            aliases: ['setrole', 's'],
            args: [{
                    key: 'member',
                    prompt: 'Kenen rooli?',
                    type: 'member'
                },
                {
                    key: 'role',
                    prompt: 'Mikä rooli luodaan?',
                    type: 'string'
                },
                {
                    key: 'channel',
                    prompt: 'Kanavan nimi?',
                    type: 'string'
                }
            ]
        });
    }

    run(msg, args) {
        const roleIds = msg.guild.settings.get("roles");
        const roles = msg.member.roles;

        const channelIds = msg.guild.settings.get("channels");

        if (roles.find(role => role.name.toLowerCase() === config.developer)) { // role exists

            if (roleIds[args.member.id]) {
                msg.channel.send("Pelaajalla on jo rooli! Käytä Tuhoa-komentoa!")
            } else {
                // luo rooli
                msg.guild.createRole({
                    name: args.role
                }).then(function (newRole) {
                    const role = newRole;
                    // adding role to database
                    roleIds[args.member.id] = role.id;
                    msg.guild.settings.set("roles", roleIds)
                    // creating the channel
                    msg.guild.createChannel(args.channel, "voice", [{
                        id: msg.guild.id,
                        deny: ["VIEW_CHANNEL", "CONNECT"]
                    }]).then(function (newChannel) {
                        // lisää kanava tietokantaan
                        channelIds[args.member.id] = newChannel.id;
                        msg.guild.settings.set("channels", channelIds);
                        // moving to the category
                        const category = msg.guild.channels.find(channel => channel.name.toLowerCase() === "ylläpidon kanavat");
                        newChannel.setParent(category).then(function (newChannel) {
                            // permissions for the role
                            newChannel.overwritePermissions(role, {
                                VIEW_CHANNEL: true,
                                CONNECT: true
                            }).then(function (newChannel) {
                                // give role to player
                                args.member.addRole(role);
                                msg.channel.send("Role asetettu! Uusi role: `" + role.name + "`! Kanava: `" + newChannel.name + "`");
                            })
                        })
                    })
                })
            }
        } else {
            msg.channel.send("Sinulla ei ole oikeuksia.")
        }
    }
};