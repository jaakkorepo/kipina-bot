const {
    Command
} = require('discord.js-commando');
const config = require('./../../config/config.json');

module.exports = class ReplyCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'poista',
            group: 'channels',
            memberName: 'poista',
            description: 'Poistaa oikeudet puhekanavalta.',
            examples: ['poista @Aarni'],
            aliases: ['removerole', 'r'],
            args: [{
                key: 'member',
                prompt: 'Keneltä poistat oikeudet?',
                type: 'member'
            }]
        });
    }

    run(msg, args) {
        const roleIds = msg.guild.settings.get("roles");
        const roleId = roleIds ? roleIds[msg.author.id] : "";
        const role = msg.guild.roles.get(roleId);
        const roles = msg.member.roles;

        if (role && (roles.find(role => config.staffRoles.includes(role.name.toLowerCase())))) { // role exists
            args.member.removeRole(roleId);
            msg.channel.send("Role poistettu käyttäjältä `" + args.member.displayName + "`!");
        } else {
            msg.channel.send("Sinulla ei ole oikeuksia tai roolia ei ole olemassa.")
        }
    }
};