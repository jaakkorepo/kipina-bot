const {
    stripIndents,
    oneLine
} = require('common-tags');
const {
	Command
} = require('discord.js-commando');

module.exports = class PrefixCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'prefix',
            group: 'util',
            memberName: 'prefix',
            description: 'Näyttää tai asettaa prefixini.',
            format: '[prefix/"default"/"none"]',
            examples: ['prefix', 'prefix -', 'prefix omg!', 'prefix default', 'prefix none'],

            args: [{
                key: 'prefix',
                prompt: 'Minkä haluat asettaa prefixiksi?',
                type: 'string',
                max: 15,
                default: ''
            }]
        });
    }

    async run(msg, args) {
        // Just output the prefix
        if (!args.prefix) {
            const prefix = msg.guild ? msg.guild.commandPrefix : this.client.commandPrefix;
            return msg.say(stripIndents `
				${prefix ? `Prefixini on \`${prefix}\`.` : 'Ei ole prefixiä.'}
				Käytä ${msg.anyUsage('command')}.
			`);
        }

        // Check the user's permission before changing anything
        if (msg.guild) {
            if (!msg.member.hasPermission('ADMINISTRATOR') && !this.client.isOwner(msg.author)) {
                return msg.say('Vain adminit saavat vaihtaa prefixiä.');
            }
        } else if (!this.client.isOwner(msg.author)) {
            return msg.say('Vain omistaja saa vaihtaa globaalin prefixin.');
        }

        // Save the prefix
        const lowercase = args.prefix.toLowerCase();
        const prefix = lowercase === 'none' ? '' : args.prefix;
        let response;
        if (lowercase === 'default') {
            if (msg.guild) msg.guild.commandPrefix = null;
            else this.client.commandPrefix = null;
            const current = this.client.commandPrefix ? `\`${this.client.commandPrefix}\`` : 'ei prefixiä';
            response = `Resetoitu prefix oletukseen (${current}).`;
        } else {
            if (msg.guild) msg.guild.commandPrefix = prefix;
            else this.client.commandPrefix = prefix;
            response = prefix ? `Prefix asetettu: \`${args.prefix}\`.` : "Prefix poistettu kokonaa.";
        }

        await msg.say(`${response} Käyttääksesi komentoja käytä ${msg.anyUsage('command')}.`);
        return null;
    }
};