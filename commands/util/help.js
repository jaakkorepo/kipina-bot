const {
    stripIndents,
    oneLine
} = require('common-tags');
const {
    Command,
    util
} = require('discord.js-commando');

module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'help',
            group: 'util',
            memberName: 'help',
            aliases: ['commands'],
            description: 'Luettelee komentoni.',
            examples: ['help', 'help prefix'],
            guarded: true,
        });
    }

    async run(msg, args) { // eslint-disable-line complexity
        const groups = this.client.registry.groups;
        const showAll = false;
        const messages = [];
        try {
            messages.push(await msg.say(stripIndents `

                **Saatavilla olevat komennot**

                ~~━━━━━━━━━━━━~~

					${(showAll ? groups : groups.filter(grp => grp.commands.some(cmd => cmd.name !== "eval")))
						.map(grp => stripIndents`
                            __${grp.name}__
                            
							${(showAll ? grp.commands : grp.commands.filter(cmd => cmd.name !== "eval"))
								.map(cmd => `**${msg.guild ? msg.guild.commandPrefix : null}${cmd.name}${cmd.format ? " " + cmd.format : ""}**: ${cmd.description}${cmd.nsfw ? ' (NSFW)' : ''}`).join('\n')
							}
                        
                            ~~━━━━━━━━━━━━~~
                            `).join('\n\n')
                    }`, {
                split: true
            }));
        } catch (err) {
            messages.push(await msg.say('oops'));
        }
        return messages
    }
};